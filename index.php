<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama : $sheep->name<br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blood : $sheep->cold_blooded <br><br>"; // "no"

$kodok = new Frog("buduk");

echo "Nama : $kodok->name<br>"; // "buduk"
echo "legs : $kodok->legs <br>"; // 4
echo "cold blood : $kodok->cold_blooded <br>"; // "no"
echo "Jump : $kodok->jump <br><br>"; // "Hip-Hop"

$sungokong = new Ape("kera sakti");

echo "Nama : $sungokong->name<br>"; // "kera sakti"
echo "legs : $sungokong->legs <br>"; // 2
echo "cold blood : $sungokong->cold_blooded <br>"; // "no"
echo "Yell : $sungokong->yell <br>"; // "Auooo"

?>